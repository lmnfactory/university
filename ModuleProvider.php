<?php

namespace Lmn\University;

use Lmn\Core\Provider\LmnServiceProvider;
use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Lmn\Core\Lib\Database\Seeder\SeederService;
use Illuminate\Support\Facades\Route;

use Lmn\University\Database\Seed\UniversitySeeder;
use Lmn\University\Database\Seed\FacultySeeder;
use Lmn\University\Database\Seed\DegreeSeeder;
use Lmn\University\Database\Seed\UniversitypersonSeeder;
use Lmn\University\Database\Seed\UniversitypersontypeSeeder;

use Lmn\Core\Lib\Cache\CacheService;
use Lmn\Core\Lib\Cache\TableCache;
use Lmn\University\Lib\Cache\UniversitypersontypeCache;
use Lmn\University\Lib\Cache\FacultyAutocompleteCache;

use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\University\Repository\Criteria\FacultyWithAllCriteria;
use Lmn\University\Repository\Criteria\FacultySearchCriteria;
use Lmn\University\Repository\Criteria\FacultyOrderCriteria;
use Lmn\University\Repository\Criteria\UniversitypersonByNameCriteria;
use Lmn\University\Repository\FacultyRepository;
use Lmn\University\Repository\UniversitypersonRepository;

class ModuleProvider implements ModuleServiceProvider{

    public function register(LmnServiceProvider $provider) {
        $app = $provider->getApp();

        $app->singleton(UniversitypersontypeCache::class, UniversitypersontypeCache::class);

        $app->singleton(FacultyRepository::class, FacultyRepository::class);
        $app->singleton(UniversitypersonRepository::class, UniversitypersonRepository::class);

        $this->registerCommands($provider);
    }

    public function boot(LmnServiceProvider $provider) {
        $app = $provider->getApp();

        $seederService = \App::make(SeederService::class);
        $seederService->addSeeder(UniversitySeeder::class);
        $seederService->addSeeder(FacultySeeder::class);
        $seederService->addSeeder(DegreeSeeder::class);
        $seederService->addSeeder(UniversitypersonSeeder::class);
        $seederService->addSeeder(UniversitypersontypeSeeder::class);

        $cacheService = \App::make(CacheService::class);
        $cacheService->add('university', function() {
            return new TableCache('university');
        });
        $cacheService->add('faculty', function() {
            return new TableCache('faculty');
        });
        $cacheService->add('degree', function() {
            return new TableCache('degree');
        });
        $cacheService->add('universitypersontype', $app->make(UniversitypersontypeCache::class));
        $cacheService->add('facultyAutocomplete', $app->make(FacultyAutocompleteCache::class));

        $criteriaService = $app->make(CriteriaService::class);
        $criteriaService->add('faculty.with.all', FacultyWithAllCriteria::class);
        $criteriaService->add('faculty.search', FacultySearchCriteria::class);
        $criteriaService->add('faculty.order', FacultyOrderCriteria::class);
        $criteriaService->add('universityperson.by.name', UniversitypersonByNameCriteria::class);
    }

    public function event(LmnServiceProvider $provider) {

    }

    public function route(LmnServiceProvider $provider) {
        Route::group(['namespace' => 'Lmn\\University\\Controller'], function() {
            Route::post('api/faculty/search','FacultyController@search');
        });
    }

    public function registerCommands($provider){

    }
}
