<?php

namespace Lmn\University\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class FacultyOrderCriteria implements Criteria {

    public function __construct() {

    }

    public function set($data) {

    }

    public function apply(Builder $builder) {
        $builder->orderBy('name', 'ASC');
    }
}
