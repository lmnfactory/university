<?php

namespace Lmn\University\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class FacultySearchCriteria implements Criteria {

    private $search;
    private $limit;

    public function __construct() {

    }

    public function set($data) {
        $this->limit = $data['limit'];
        $this->search = $data['search'];
    }

    public function apply(Builder $builder) {
        $self = $this;
        $builder->where(function($query) use ($self){
            $query->where('faculty.code', 'like', '%'.$self->search.'%')
                ->orWhere('faculty.name', 'like', '%'.$self->search.'%');
        })
        ->limit($this->limit);
    }
}
