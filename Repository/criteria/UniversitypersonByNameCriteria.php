<?php

namespace Lmn\University\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class UniversitypersonByNameCriteria implements Criteria {

    private $name;

    public function __construct() {

    }

    public function set($data) {
        $this->name = $data['name'];
    }

    public function apply(Builder $builder) {
        $builder->where('universityperson.name', '=', $this->name);
    }
}
