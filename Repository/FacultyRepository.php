<?php

namespace Lmn\University\Repository;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\University\Database\Model\Faculty;

class FacultyRepository extends AbstractEloquentRepository {

    private $saveService;

    public function __construct(CriteriaService $criteriaService, SaveService $saveService) {
        parent::__construct($criteriaService);
        $this->saveService = $saveService;
    }

    public function getModel() {
        return Faculty::class;
    }

    public function create($data) {
        $model = $this->getModel();
        $entry = new $model();
        $entry->fill($data);

        $this->saveService->begin($entry)
            ->transaction();

        return $entry;
    }
}
