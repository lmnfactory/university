<?php

namespace Lmn\University\Lib\Cache;

use Lmn\Core\Lib\Cache\Cacheable;
use Lmn\University\Repository\FacultyRepository;

class FacultyAutocompleteCache implements Cacheable {

    private $repo;

    public function __construct(FacultyRepository $repo) {
        $this->repo = $repo;
    }

    public function cache() {
        $list = $this->repo->clear()
            ->criteria('faculty.order')
            ->criteria('faculty.with.all')
            ->criteria('core.limit', ['limit' => 30])
            ->all();

        return $list;
    }
}
