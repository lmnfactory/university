<?php

namespace Lmn\University\Lib\Cache;

use Lmn\Core\Lib\Cache\Cacheable;
use Lmn\Core\Repository\AppoptionsRepository;

class UniversitypersontypeCache implements Cacheable {

    private $repo;

    public function __construct(AppoptionsRepository $appoptionsRepo) {
        $this->repo = $appoptionsRepo;
    }

    public function cache() {
        $options = $this->repo->clear()
            ->criteria('core.option.namespace', ['namespace' => 'lmn.university.persontype.'])
            ->all();

        $optionArray = [];
        foreach ($options as $o) {
            $exp = explode('.', $o->name);
            $name = $exp[sizeof($exp) - 1];
            $optionArray[$name] = $o->value;
        }

        return $optionArray;
    }
}
