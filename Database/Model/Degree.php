<?php

namespace Lmn\University\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Degree extends Model {

    protected $table = 'degree';

    protected $fillable = ['name', 'code'];
}
