<?php

namespace Lmn\University\Database\Model;

use Illuminate\Database\Eloquent\Model;

class University extends Model {

    protected $table = 'university';

    protected $fillable = ['name', 'code', 'city_id'];
}
