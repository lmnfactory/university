<?php

namespace Lmn\University\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Universityperson extends Model {

    protected $table = 'universityperson';

    protected $fillable = ['name'];
}
