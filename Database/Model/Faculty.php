<?php

namespace Lmn\University\Database\Model;

use Illuminate\Database\Eloquent\Model;
use Lmn\University\Database\Model\University;

class Faculty extends Model {

    protected $table = 'faculty';

    protected $fillable = ['university_id', 'name', 'code'];

    public function university() {
        return $this->belongsTo(University::class);
    }
}
