<?php

namespace Lmn\University\Database\Seed;

use Illuminate\Database\Seeder;

class DegreeSeeder extends Seeder {

    public function run() {
        \DB::table('degree')->insert([
            [
                'id' => 1,
                'name' => 'Bakalárksy',
                'code' => 'Bc'
            ],
            [
                'id' => 2,
                'name' => 'Inžiniersky',
                'code' => 'Ing'
            ],
            [
                'id' => 3,
                'name' => 'Magisterský',
                'code' => 'Mgr'
            ],
            [
                'id' => 4,
                'name' => 'Doktorantský',
                'code' => 'PhD'
            ],
            [
                'id' => 5,
                'name' => 'Doktorský',
                'code' => 'MUDr'
            ]
        ]);
    }
}
