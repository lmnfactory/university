<?php

namespace Lmn\University\Database\Seed;

use Illuminate\Database\Seeder;

class UniversitySeeder extends Seeder {

    public function run() {
        \DB::table('university')->insert([
            [
                'id' => 1,
                'name' => 'Slovenská technická univerzita',
                'code' => 'STU',
                'city_id' => 1
            ],
            [
                'id' => 2,
                'name' => 'Univerzita Komenského',
                'code' => 'UK',
                'city_id' => 1
            ],
            [
                'id' => 3,
                'name' => 'Ekonomická univerzita',
                'code' => 'EUBA',
                'city_id' => 1
            ]
        ]);
    }
}
