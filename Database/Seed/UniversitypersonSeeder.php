<?php

namespace Lmn\University\Database\Seed;

use App;
use Illuminate\Database\Seeder;

class UniversitypersonSeeder extends Seeder {

    public function run() {
        $env = App::environment();
        if ($env == "production") {
            return;
        }

        \DB::table('universityperson')->insert([
            [
                'id' => 1,
                'name' => 'Pavol Navrat'
            ],
            [
                'id' => 2,
                'name' => 'Ivan Polasek'
            ],
            [
                'id' => 3,
                'name' => 'Majstro Picasso'
            ]
        ]);
    }
}
