<?php

namespace Lmn\University\Database\Seed;

use Illuminate\Database\Seeder;

class FacultySeeder extends Seeder {

    public function run() {
        \DB::table('faculty')->insert([
            [
                'id' => 1,
                'university_id' => 1,
                'name' => 'Fakulta informatiky a informačných technológií',
                'code' => 'FIIT'
            ],
            [
                'id' => 2,
                'university_id' => 1,
                'name' => 'Fakulta elektrotechniky a informatiky',
                'code' => 'FEI'
            ],
            [
                'id' => 3,
                'university_id' => 1,
                'name' => 'Fakulta chemickej a potravinárskej technológie',
                'code' => 'FCHPT'
            ],
            [
                'id' => 4,
                'university_id' => 2,
                'name' => 'Lekárska fakulta UK',
                'code' => 'LF UK'
            ],
            [
                'id' => 5,
                'university_id' => 2,
                'name' => 'Právnická fakulta UK',
                'code' => 'PraF UK'
            ],
            [
                'id' => 6,
                'university_id' => 2,
                'name' => 'Prírodovedecká fakulta UK',
                'code' => 'PriF UK'
            ],
            [
                'id' => 7,
                'university_id' => 3,
                'name' => 'Národohospodárska fakulta',
                'code' => 'NHF'
            ],
            [
                'id' => 8,
                'university_id' => 3,
                'name' => 'Fakulta hospodárskej informatiky',
                'code' => 'FHI'
            ],
            [
                'id' => 9,
                'university_id' => 3,
                'name' => 'Fakulta podnikového manažmentu',
                'code' => 'FPM'
            ]
        ]);
    }
}
