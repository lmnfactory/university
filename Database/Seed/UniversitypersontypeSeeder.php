<?php

namespace Lmn\University\Database\Seed;

use Illuminate\Database\Seeder;

class UniversitypersontypeSeeder extends Seeder {

    public function run() {
        \DB::table('appoptions')->insert([
            [
                'name' => 'lmn.university.persontype.professor',
                'value' => '1'
            ],
            [
                'name' => 'lmn.university.persontype.lecturer',
                'value' => '2'
            ]
        ]);
    }
}
