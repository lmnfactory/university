<?php

namespace Lmn\University\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Model\ValidationService;

use Lmn\University\Repository\FacultyRepository;

class FacultyController extends Controller {

    public function search(Request $request, ResponseService $responseService, FacultyRepository $facultyRepo) {
        $data = $request->json()->all();

        if (!isset($data['search']) || $data['search'] == null) {
            $data['search'] = "";
        }

        $data['limit'] = 10;
        $faculties = $facultyRepo->clear()
            ->criteria('faculty.with.all')
            ->criteria('faculty.search', [
                'limit' => 10,
                'search' => $data['search']
            ])
            ->all();

        return $responseService->response($faculties);
    }
}
